package com.fag.mytrello.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "users")
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "email", nullable = false, unique = true)
    @Length(min = 3, max = 255)
    private String email;

    @Column(name = "password", nullable = false)
    @Length(min = 6, max = 255)
    private String password;

    @Column(name = "first_name", nullable = false)
    @Length(min = 1, max = 50)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    @Length(min = 1, max = 50)
    private String lastName;

    @Column(name = "ban_start_date")
    private LocalDateTime banStartDate;

    @Column(name = "ban_length")
    @Range(min = 1)
    private short banLength;

    @Column(name = "registration_date", nullable = false)
    private LocalDateTime registrationDate;

    public User() {
    }

}
