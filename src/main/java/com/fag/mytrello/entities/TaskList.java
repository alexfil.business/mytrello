package com.fag.mytrello.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "task_lists")
@Getter
@Setter
public class TaskList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", nullable = false)
    @Length(min = 1, max = 50)
    private String name;

    @Column(name = "position")
    @Range(min = 0)
    private short position;

    // Relations

    @ManyToOne
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

    public TaskList() {
    }
}
