package com.fag.mytrello.entities;

import com.fag.mytrello.models.enumerations.ProjectVisibility;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "projects")
@Getter
@Setter
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", nullable = false)
    @Length(min = 1, max = 50)
    private String name;

    @Column(name = "description", nullable = false)
    @Length(min = 1, max = 255)
    private String description;

    @Column(name = "creation_date", nullable = false)
    private LocalDateTime creationDate;

    @Column(name = "visibility", nullable = false)
    @Enumerated(EnumType.STRING)
    private ProjectVisibility projectVisibility;

    public Project() {
    }
}
