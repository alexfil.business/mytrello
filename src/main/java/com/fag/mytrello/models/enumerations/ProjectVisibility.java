package com.fag.mytrello.models.enumerations;

public enum ProjectVisibility {
    PRIVATE,
    PUBLIC
}
