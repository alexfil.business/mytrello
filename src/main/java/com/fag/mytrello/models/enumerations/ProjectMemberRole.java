package com.fag.mytrello.models.enumerations;

public enum ProjectMemberRole {
    UNKNOWN,
    NEW,
    MEMBER,
    JOIN_US_REQUEST,
    TAKE_ME_REQUEST,
    BANNED,
    REMOVED
}
